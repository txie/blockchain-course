#!/bin/bash

geth --dev \
--datadir "/usr/blockchain-course/ethdev" \
--mine --minerthreads=2 \
--networkid 94555 \
--verbosity 3 \
--rpc \
--port 30301 \
--rpcport 8545 \
--rpcaddr 0.0.0.0 \
--rpccorsdomain "*" console 2>> ethdev.log