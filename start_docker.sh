#!/bin/bash
docker run \
-v ~/Workspace/blockchainV/ethdev:/usr/blockchain-course/ethdev \
-v ~/Workspace/blockchainV/playground:/usr/blockchain-course/playground \
-p 9545:8545 \
-p 9080:8080 \
-p 9000:8000 \
-p 90:80 \
-it txie/blockchain-course /bin/bash
