pragma solidity ^0.4.24;


contract Calculator {
    int public result;
    string public name;

    constructor(string _name) public {
        name = _name;
    }

    function setName(string _name) public {
        name = _name;
    }

    function add(int a, int b) public returns (int) {
        result = a+b;
        return result;
    }
    
    function subtract(int a, int b) public returns (int) {
        result = a-b;
        return result;
    }
    
    // view function
    function getResult() view public returns (int) {
        return result;
    }
    
    // pure function
    function max(int a, int b) public pure returns (int) {
        return (a > b) ? a : b;
    }
    
}
