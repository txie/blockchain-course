pragma solidity ^0.4.17;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Calculator.sol";

contract TestCalculator {
    Calculator calc = Calculator(DeployedAddresses.Calculator());
    function testAdd() public {
        int returned = calc.add(3, 10);
        int expected = 13;
        Assert.equal(returned, expected, "Calculator should return 13");
    }

    function testSubtract() public {
        int returned = calc.subtract(10, 3);
        int expected = 7;
        Assert.equal(returned, expected, "Calculator should return 7");      
    }

    function testMax() public {
        int returned = calc.max(10, 3);
        int expected = 10;
        Assert.equal(returned, expected, "Calculator should return 10");      
    }
}
