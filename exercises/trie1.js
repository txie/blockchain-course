//Just importing the requirements
var Trie = require('merkle-patricia-tree/secure');
var levelup = require('levelup');
var leveldown = require('leveldown');
var RLP = require('rlp');
var assert = require('assert');

// Connecting to the leveldb database
// REPLACE IT
var db = levelup(leveldown('/Users/txie/Workspace/blockchainV/ethdev/geth/chaindata'));

// Adding the "stateRoot" value from the block so that we can inspect the state root at that block height.
// REPLACE IT
var root = '0x35a8935b6f4bebe9d88c1a45c2a96c7f4b5a358b4609d6be62b0906f3c66cd32';

//Creating a trie object of the merkle-patricia-tree library
var trie = new Trie(db, root);

//Creating a nodejs stream object so that we can access the data
var stream = trie.createReadStream()

//Turning on the stream (because the node js stream is set to pause by default)
stream.on('data', function (data){
  //printing out the keys of the "state trie"
  console.log(data.key);
});
