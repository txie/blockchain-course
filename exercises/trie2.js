//Getting the requirements
var Trie = require('merkle-patricia-tree/secure');
var levelup = require('levelup');
var leveldown = require('leveldown');
var utils = require('ethereumjs-util');
var BN = utils.BN;
var Account = require('ethereumjs-account');

//Connecting to the leveldb database
var db = levelup(leveldown('/Users/txie/Workspace/blockchainV/ethdev/geth/chaindata'));

//Adding the "stateRoot" value from the block so that we can inspect the state root at that block height.
var root = '0x35a8935b6f4bebe9d88c1a45c2a96c7f4b5a358b4609d6be62b0906f3c66cd32';

//Creating a trie object of the merkle-patricia-tree library
var trie = new Trie(db, root);

var address = '0xe91d75d6d68169789f4dedb22a6f0ea7ebdd6c1b';
trie.get(address, function (err, raw) {
    if (err) return cb(err)
    //Using ethereumjs-account to create an instance of an account
    var account = new Account(raw)
    console.log('Account Address: ' + address);
    //Using ethereumjs-util to decode and present the account balance 
    console.log('Balance: ' + (new BN(account.balance)).toString());
})