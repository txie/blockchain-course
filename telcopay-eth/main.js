// CHANGE 'account' values
var accounts = {
    'alice': {
        firstName: 'Alice',
        lastName: 'Jones',
        emailAddress: 'ajones@telcopay.com',
        phoneNumber: '(650) 430-9266',
        password: 'test', 
        carrierName: "att",
        account: '0xe91d75d6d68169789f4dedb22a6f0ea7ebdd6c1b'                
    },

    'bob': {
        firstName: 'Bob',
        lastName: 'Green',
        password: 'test',
        emailAddress: 'bgreen@telcopay.com',
        phoneNumber: '(650) 430-9266',
        carrierName: "verizon",
        account: '0x1678eb793da450bf386ae584c3720cdd485a428e'
    }, 

    'xiaohong': {
        firstName: 'Xiaohong',
        lastName: 'Liu',
        password: 'test',
        emailAddress: 'xliu@telcopay.com',
        phoneNumber: '189-3560988',
        carrierName: "china-telecom",
        account: '0x4112fc837328e225cf6a0443ca62923f44728cba'
    },

    'jose': {
        firstName: 'Jose',
        lastName: 'Gomez',
        password: 'test',
        emailAddress: 'jgomez@telcopay.com',
        phoneNumber: '(632) 816-9266',
        carrierName: "pldt",
        account: '0x71524fd8d2c118a1cbd35d4e24ecfcb162fd8437'
    },

    'charlie': {
        firstName: 'Charlie',
        lastName: 'Klein',
        emailAddress: 'cklein@telcopay.com',
        phoneNumber: '(650) 410-3011',
        password: 'test',
        account: '0x27e947b0c0353301930151f6b708dd5686934a89'                
    },
    'amy': {
        firstName: 'Amy',
        lastName: 'Jackson',
        password: 'test',
        emailAddress: 'amy@telcopay.com',
        phoneNumber: '(510) 450-3013',
        account: '0xc380ba85967c55266e9a5bfc7c8a528f5b721d6f',
    } 
}

var meta = {
    'corpName': 'TelcoPay Inc.',
    'corpLogo': 'telcopay-logo.png',
    'pldt': 'pldt-logo.png'
}

// CHANGE THIS if necessary
const devnet_url = "http://localhost:9545";

// CHANGE THIS: copy it from Remix "Run" tab
const tokenAddr = '0xfd7c37e59303d25a28ddc4b58f241308781d33f7';

// CHANGE THIS if necessary
const transTokenContractContent = [{"constant":true,"inputs":[],"name":"getMyEthBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"receiver","type":"address"},{"name":"amount","type":"uint256"}],"name":"sendToken","outputs":[{"name":"sufficient","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getMyBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"act","type":"address"}],"name":"getEthBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"setEthValue","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"tokenBalanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"act","type":"address"}],"name":"getBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_symbol","type":"string"},{"name":"supply","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"sender","type":"address"},{"indexed":false,"name":"receiver","type":"address"},{"indexed":false,"name":"amount","type":"uint256"}],"name":"TokenTransfer","type":"event"}];
const devTokenContractContent = [{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"_totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"}];
tokenContent = devTokenContractContent;

// var Web3 = require('web3');
var web3 = new Web3();

web3 = (typeof web3 !== 'undefined') 
    ? new Web3(web3.currentProvider) 
    : new Web3(new Web3.providers.HttpProvider(devnet_url));
web3.setProvider(new web3.providers.HttpProvider(devnet_url));

console.log('devnet_url: ' + devnet_url + ', web3.verion.api:' + web3.version.api);

function getWeb3() {
    return web3;
}

function getDeployedTokenContract() {
    const contract = web3.eth.contract(tokenContent);
    return contract.at(tokenAddr);
}
