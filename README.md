# blockchain-course

Start docker
docker run -p 9545:8545 -p 9080:8080 -p 9000:8000 -p 9040:8040 -it txie/blockchain-course /bin/bash

Manual Start Sequence
1. start_geth.sh (rpcport: 8545)
2. start remix-ide (port: 8080)
3. start eth-explorer (port: 8000)
4. start light-server (port: 8040)
    start_light-server.sh 


