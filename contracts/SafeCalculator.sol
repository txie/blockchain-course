pragma solidity ^0.4.24;
import "github.com/OpenZeppelin/zeppelin-solidity/contracts/math/SafeMath.sol";

contract SafeCalculator {
    string name;
    uint result;

    constructor(string _name) public {
      name = _name;
    }
    function setName(string _name) public {
        name = _name;
    }

    function add(uint a, uint b) public returns (uint) {
        result = SafeMath.add(a, b);
        return result;
    }
    
    function subtract(uint a, uint b) public returns (uint) {
        result = SafeMath.sub(a, b);
        return result;
    }
    
    function divide(int a, int b) pure public returns (int) {
        return a / b;
    }
    
    function safeDivide(uint a, uint b) public returns (uint) {
        result = a / b;
        return result;
    }
    // view function
    function getResult() view public returns (uint) {
        return result;
    }
    
    // pure function
    function max(int a, int b) public pure returns (int) {
        return (a > b) ? a : b;
    }
    
}

