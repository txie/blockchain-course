pragma solidity ^0.4.23;


contract TransToken { 
    mapping (address => uint) public tokenBalanceOf;
    string public symbol;
    
    event TokenTransfer(address sender, address receiver, uint amount);
  
    /* Initializes contract with initial supply tokens to the creator of the contract */
    constructor(string _symbol, uint supply) public {
        symbol = _symbol;
        tokenBalanceOf[msg.sender] = supply;
    }
  
    function transfer(address receiver, uint amount) public returns(bool sufficient) {
        if (tokenBalanceOf[msg.sender] < amount) return false;
        tokenBalanceOf[msg.sender] -= amount;
        tokenBalanceOf[receiver] += amount;
        emit TokenTransfer(msg.sender, receiver, amount);
        return true;
    }

    function balanceOf(address act) view public returns (uint) {
        return tokenBalanceOf[act];
    }    

    function getMyBalance() view public returns (uint) {
        return tokenBalanceOf[msg.sender];
    }

    function getMyEthBalance() view public returns (uint) {
        return address(this).balance;
    }
    function getEthBalance(address act) view public returns (uint) {
        return act.balance;
    }
}